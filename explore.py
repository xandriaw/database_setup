import numpy as np
from numpy import random
import pandas as pd
from geopy.geocoders import Nominatim
from random import randrange
import datetime

geolocator = Nominatim()
targets = pd.DataFrame()
targets['target_name'] = ["Kolkata, India", "Guangzhou, China", "Hyderabad, India", "Tianjin, China", "Hanoi, Vietnam",
           "Beijing, China", "Yokohama, Japan", "Surat, India", "Karachi, Pakistan", "Berlin, Germany",
           "Baghdad, Iraq", "Seoul, South Korea", "Abidjan, Ivory Coast", "Chennai, India", "Jakarta, Indonesia",
           "Singapore", "Addis Ababa, Ethiopia", "Johannesburg, South Africa", "Yangon, Myanmar", "Moscow, Russia",
           "Mexico City, Mexico", "Nairobi, Kenya"]
targets['target_location'] = targets['target_name'].apply (geolocator.geocode)
startday=datetime.datetime(2017, 12, 6, 00, 00, 00, 000000)
D=60
for day in range(0, D):
    N = random.randint(30,50)  # how many images per day
    hours = 24
    # create my empty dataframe with coordinates
    # want LLA and city name as target
    locations = pd.DataFrame(
        columns=['target_name', 'target_id', 'center_latitude', 'center_longitude', 'center_altitude', 'timestamp'])
    for i in range(0, N):
        target_id = random.randint(0, targets.__len__())
        target_name = targets['target_name'].iloc[target_id]
        location = targets['target_location'].iloc[target_id]
        locations.loc[i] = {'target_id': target_id,
                            'target_name': target_name,
                            'center_latitude': float(location.latitude + random.rand(1) * .5),
                            'center_longitude': float(location.longitude + random.rand(1) * .5),
                            'center_altitude': float(location.altitude + random.rand(1) * .5),
                            'timestamp': startday + datetime.timedelta(days=day,
                                seconds=randrange(3600 * hours))}  # generates timestamps over the last day

    if (day == 0):
        headers = locations
    else:
        headers=headers.append(locations)

def get_filename(x):
    time_stripped = datetime.date.strftime(x['timestamp'], '%Y%M%d%H%M%S%f')
    filename= 'EO_' + str(x['target_id']) + '_' + str(time_stripped)
    return filename
# note that the timestamps are not in order, but it doesn't really matter for our purposes for now.
#  we can sort when we display
headers['filename']= headers.apply(get_filename, axis=1)

bot=pd.DataFrame()
bot['bot_out_timestamp']=headers['timestamp'].apply(lambda x: x+datetime.timedelta(seconds=random.randint(low=1, high=20)))
bot['filename']= headers['filename']
from scipy.stats import truncnorm
def get_truncated_normal(mean=0, sd=1, low=0, upp=10):
    return truncnorm(
        (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)
X=get_truncated_normal(6,1,0,10)

bot['NIIRS'] =X.rvs(bot.shape[0])

# this assumes uncertainty mean is zero or unbiased
bot['uncertainty'] = random.normal(0, .07,  bot.shape[0])
bot['predicted'] = bot['NIIRS'].apply(lambda x: x +random.normal(loc=0, scale =0.05))

from sqlalchemy import create_engine
import pymysql

engine = create_engine("mysql://root:neurocoreai@containers_aidb_1/test")
headers.to_sql(con=engine, name='header_data', if_exists='replace')
bot.to_sql(con=engine, name='NIIRS_robot_out', if_exists='replace')

