#get the list of images from googleimagesdownload
#save them with filenames that match our fake images in the database
import os
import urllib
from numpy import random
os.chdir('/home/docker/R-projects/images')


from sqlalchemy import create_engine
import pymysql
engine = create_engine("mysql://root:neurocoreai@172.17.0.2/demodb")

result = engine.execute("select filename from demodb.img_metadata")
filenames=[]
for row in result:
    filenames.append(row[0])
print filenames

original_names= os.listdir('/home/docker/R-projects/images')

N=original_names.__len__()

import shutil
i=1
for filename in filenames:
    if i>=N-1:
        shutil.copy(original_names[i], filename+ '.jpg')
    else:
        os.rename(original_names[i], filename+ '.jpg')
    i=i+1
